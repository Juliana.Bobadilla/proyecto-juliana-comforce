/*
|--------------------------------------------------------------------------
| VALIDACIONES PROCESO
|--------------------------------------------------------------------------
*/

$('#form_proceso').submit(function(event) {
  var response = true;

  if ($('#numeroProceso').val() == '') {
    toastr.error("El campo Numero de Proceso es obligatorio", "Error");
    return false;
  }

  if ($('#descripcionProceso').val() == '') {
    toastr.error("El campo Descripción de Proceso es obligatorio", "Error");
    return false;
  }

  if ($('#idSede').val().trim() === '') {
    toastr.error("El campo Sede es obligatorio", "Error");
    return false;
  }

  if ($('#presupuestoProceso').val() == '') {
    toastr.error("El campo Presupuesto de Proceso es obligatorio", "Error");
    return false;
  }

  var pres = $('#numeroProceso').val();
  if(pres.length != 8){
    toastr.error("El campo Numero de Proceso debe tener 8 carecteres", "Error");
    return false;
  }

  var desc = $('#descripcionProceso').val();
  var caracteres = desc.length;
  if(desc.length >200){
    toastr.error("El campo Descripción debe tener máximo 200 carecteres. Digito "+caracteres+" caracteres", "Error");
    return false;
  }

});

//MASCARA PARA EL CAMPO DE PRESUPUESTO EN PESOS COLOMBIANOS
$('#presupuestoProceso').mask('0.000.000.000.000,00', {reverse: true});

//MASCARA PARA EL CAMPO DE PRESUPUESTO EN DOLARES
$('#presupuestodolarProceso').mask('0.000.000.000.000,00', {reverse: true});

