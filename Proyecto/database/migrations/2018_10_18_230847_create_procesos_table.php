<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procesos', function (Blueprint $table) {
            $table->increments('id');
            $table->String('numeroProceso')->unique();
            $table->String('descripcionProceso', 200);
            $table->String('fechaProceso');
            $table->decimal('presupuestoProceso',15,2);
            $table->decimal('presupuestodolarProceso',15,2);
            $table->Integer('idSede')->unsigned();
            $table->foreign('idSede')->references('id')->on('sedes');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procesos');
    }
}
