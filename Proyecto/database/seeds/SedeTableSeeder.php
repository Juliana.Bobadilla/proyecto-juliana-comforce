<?php

use Illuminate\Database\Seeder;
use App\Models\Sede as Sede;

class SedeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sede::create([
            'id'          => 1,
            'nombreSede'  => 'Bogotá',
        ]);

        Sede::create([
            'id'          => 2,
            'nombreSede'  => 'México',
        ]);

        Sede::create([
            'id'          => 3,
            'nombreSede'  => 'Perú',
        ]);
    }
}
