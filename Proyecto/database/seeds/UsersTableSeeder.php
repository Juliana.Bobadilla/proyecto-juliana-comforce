<?php

use Illuminate\Database\Seeder;
use App\User as User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'id'          => 1,
            'name'  => 'Juliana Bobadilla',
            'email'  => 'prueba@gmail.com',
            'password'    =>  bcrypt('prueba.2018'),
        ]);
    }
}
