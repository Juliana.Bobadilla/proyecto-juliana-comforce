<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/*
|--------------------------------------------------------------------------
| RUTAS PARA LAS SEDES DEL SISTEMA
|--------------------------------------------------------------------------
*/
  Route::get('sedes', 'SedeController@index')->name('sedes.index');

  Route::get('sedes/create', 'SedeController@create')->name('sedes.create');

  Route::post('sedes/store', 'SedeController@store')->name('sedes.store');

  Route::get('sedes/{sede}/edit', 'SedeController@edit')->name('sedes.edit');

  Route::put('sedes/{sede}', 'SedeController@update')->name('sedes.update');

  Route::delete('sedes/{sede}', 'SedeController@destroy')->name('sedes.destroy');


  /*
|--------------------------------------------------------------------------
| RUTAS PARA LOS PROCESOS DEL SISTEMA
|--------------------------------------------------------------------------
*/
  Route::get('procesos', 'ProcesoController@index')->name('procesos.index');

  Route::get('procesos/create', 'ProcesoController@create')->name('procesos.create');

  Route::post('procesos/store', 'ProcesoController@store')->name('procesos.store');

  Route::get('procesos/{proceso}/edit', 'ProcesoController@edit')->name('procesos.edit');

  Route::get('procesos/{proceso}/vertodo', 'ProcesoController@vertodo')->name('procesos.vertodo');

  Route::put('procesos/{proceso}', 'ProcesoController@update')->name('procesos.update');

  Route::delete('procesos/{proceso}', 'ProcesoController@destroy')->name('procesos.destroy');