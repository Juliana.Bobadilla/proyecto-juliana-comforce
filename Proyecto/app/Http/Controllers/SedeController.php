<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Sede as Sede;

class SedeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sedes = Sede::select('sedes.*')->get();

      return view('Sede/list', compact('sedes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Sede/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sedes = new Sede;
        $sedes->create($request->all());

        return redirect('sedes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sedes = Sede::find($id);

       return view('Sede/update', compact('sedes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $sedes = Sede::find($id);

        $sedes->update($request->all());

        return redirect('sedes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sedes = Sede::select('sedes.*' , 'procesos.idSede')
                        ->leftjoin('procesos' , 'procesos.idSede', '=' , 'sedes.id')
                        ->where('sedes.id' , '=' , $id)
                        ->get();

        foreach ($sedes as $Sede) {
           if ($Sede->idSede != null) {
                $notification = array(
                  'message' => 'No es posible eliminar esta Sede ya que se encuentra asignada a un Proceso', 
                  'alert-type' => 'error'
                  );
                return redirect('sedes')->with($notification);
           }
           else{
                $sedes = Sede::find($id)->delete();

                $notification = array(
                  'message' => 'Sede Eliminada correctamente', 
                  'alert-type' => 'success'
                  );           
                return redirect('sedes')->with($notification);
           }
        }   
    }
}
