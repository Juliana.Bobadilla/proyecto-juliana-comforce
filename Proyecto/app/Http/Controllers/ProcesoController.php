<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Sede as Sede;

use App\Models\Proceso as Proceso;

class ProcesoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $procesos = Proceso::select('procesos.*', 'sedes.nombreSede')
                        ->join('sedes' , 'sedes.id', '=' , 'procesos.idSede')
                        ->get();

        return view('Proceso/list', compact('procesos'));
    }

    public function vertodo($id)
    {
        $procesos = Proceso::select('procesos.*', 'sedes.nombreSede')
                        ->join('sedes' , 'sedes.id', '=' , 'procesos.idSede')
                        ->where('procesos.id' , '=' , $id)
                        ->get();

        return view('Proceso/ver_todo', compact('procesos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $sedes = Sede::select('sedes.*')->get();


        return view('Proceso/create', compact('sedes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $procesos = Proceso::select('procesos.*')->where('procesos.numeroProceso', '=' , $request->numeroProceso)->get();

        if ($procesos->isEmpty()) {

            $quitar_punto =  str_replace(".", "", $request->presupuestoProceso);
            $quitar_coma = str_replace(",", ".", $quitar_punto);
            $dolar = $quitar_coma / 3084.25; 

            $Proceso = new Proceso;
            $Proceso->numeroProceso = $request->numeroProceso;
            $Proceso->descripcionProceso = $request->descripcionProceso;
            $Proceso->fechaProceso = date('d/m/Y');
            $Proceso->presupuestoProceso = $quitar_coma;
            $Proceso->presupuestodolarProceso = $dolar;
            $Proceso->idSede = $request->idSede;
            $Proceso->save();

            return redirect('procesos');
        }
        if (!$procesos->isEmpty()){
            $notification = array(
              'message' => 'El numero de proceso digitado ya se encuentra registrado para otro Proceso',
              'alert-type' => 'error'
              );
            return redirect()->back()->with($notification);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $procesos = Proceso::find($id);

        $sedes = Sede::select('sedes.*')->get();

        return view('Proceso/update', compact('procesos', 'sedes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $procesos = Proceso::select('procesos.*')->where('procesos.numeroProceso', '=' , $request->numeroProceso)->where('procesos.id', '!=' , $id)->get();

        if ($procesos->isEmpty()) {

            $quitar_punto =  str_replace(".", "", $request->presupuestoProceso);
            $quitar_coma = str_replace(",", ".", $quitar_punto);
            $dolar = $quitar_coma / 3084.25; 

            $Proceso =  Proceso::find($id);
            $Proceso->numeroProceso = $request->numeroProceso;
            $Proceso->descripcionProceso = $request->descripcionProceso;
            $Proceso->presupuestoProceso = $quitar_coma;
            $Proceso->presupuestodolarProceso = $dolar;
            $Proceso->idSede = $request->idSede;
            $Proceso->save();

            return redirect('procesos');
        }

        if (!$procesos->isEmpty()){
            $notification = array(
              'message' => 'El numero de proceso digitado ya se encuentra registrado para otro Proceso',
              'alert-type' => 'error'
              );
            return redirect()->back()->with($notification);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $procesos = Proceso::select('procesos.*')
                                ->where('procesos.id', '=' , $id)
                                ->delete();

        $notification = array(
          'message' => 'Proceso Eliminado correctamente', 
          'alert-type' => 'success'
        );           
        return redirect('procesos')->with($notification);                        
    }
}
