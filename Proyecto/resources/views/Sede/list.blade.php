@extends('layouts.app')

@section('content')
    <div id="fondo" class="container">
    	<div class="row">
    		<h4 class="with-border">Sede</h4>
    		<div class="col-md-4 col-md-offset-7 separacion">
	            <a href="{{ route('sedes.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Agregar</a>
	        </div>
    		<div class="col-sm-10 col-md-offset-1">				
    			<table id="tabla" class="table table-bordered display compact cell-border row-border" style="width:100%; text-align: center">
			        <thead>
			          <tr>
			            <th id="titulo">Nombre</th>
			            <th id="titulo">Editar</th>
			            <th id="titulo">Eliminar</th>
			          </tr>
			        </thead>
			        <tbody>
			          @foreach($sedes as $Sede)
			            <tr>
			               <td>{{$Sede->nombreSede}}</td>
			               <td width="10px">
			                  <a href="{{ route('sedes.edit', $Sede->id) }}" class="btn btn-primary"><i class="fas fa-edit" aria-hidden="true"></i></a>
			                </td>
			                <td width="10px">
			                  {!! Form::open(['route' => ['sedes.destroy', $Sede->id], 'method' => 'DELETE']) !!}
			                    <button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
			                  {!! Form::close() !!}
			                </td>
			            </tr>
			          @endforeach
			        </tbody>
			    </table>
    		</div>
    	</div>    	
    </div>
@endsection