@extends('layouts.app')
@section('content')
    <div id="fondo" class="container">
    	<h4 class="with-border">Actualizar Sede</h4>
    	{!! Form::model($sedes, ['route' => ['sedes.update', $sedes->id],'method' => 'PUT', 'id' => 'form_sede']) !!}
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Nombre Sede</label>
	                    <div class="col-md-6">
	                        <input id="nombreSede" type="text" class="form-control" name="nombreSede" value="{{ $sedes->nombreSede}}">
	                    </div>
	                </div>
	                
	    		</div>
	    	</div>  
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-5 separacion">
			        <button class="btn btn-primary">Guardar</button>
			    </div>
	    	</div> 
    	{{ Form::close() }} 	
    </div>
<script src="{{asset('js/sede.js')}}"></script> 
@endsection