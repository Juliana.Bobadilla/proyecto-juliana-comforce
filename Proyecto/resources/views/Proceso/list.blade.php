@extends('layouts.app')
@section('content')
    <div id="fondo" class="container">
    	<div class="row">
    		<h4 class="with-border">Procesos</h4>
    		<div class="col-md-4 col-md-offset-7 separacion">
	            <a href="{{ route('procesos.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus" aria-hidden="true"></i> Agregar Proceso</a>
	        </div>
    		<div class="col-sm-10 col-md-offset-1">				
    			<table id="tabla" class="table table-bordered display compact cell-border row-border" style="width:100%; text-align: center">
			        <thead>
			          <tr>
			            <th id="titulo">Consecutivo</th>
			            <th id="titulo">Numero Proceso</th>
			            <th id="titulo">Fecha Creación</th>
			            <th id="titulo">Ver Datos</th>
			            <th id="titulo">Editar</th>
			            <th id="titulo">Eliminar</th>
			          </tr>
			        </thead>
			        <tbody>
			          @foreach($procesos as $Proceso)
			            <tr>
			            	<td>{{$Proceso->id}}</td>
			            	<td>{{$Proceso->numeroProceso}}</td>
			            	<td>{{$Proceso->fechaProceso}}</td>
			            	<td width="10px">
			                  <a href="{{ route('procesos.vertodo', $Proceso->id) }}" class="btn btn-info"><i class="fa fa-list-alt" aria-hidden="true"></i></a>
			                </td>
			               	<td width="10px">
			                  <a href="{{ route('procesos.edit', $Proceso->id) }}" class="btn btn-primary"><i class="fas fa-edit" aria-hidden="true"></i></a>
			                </td>
			                <td width="10px">
			                  {!! Form::open(['route' => ['procesos.destroy', $Proceso->id], 'method' => 'DELETE']) !!}
			                    <button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button>
			                  {!! Form::close() !!}
			                </td>
			            </tr>
			          @endforeach
			        </tbody>
			    </table>
    		</div>
    	</div>    	
    </div>
@endsection