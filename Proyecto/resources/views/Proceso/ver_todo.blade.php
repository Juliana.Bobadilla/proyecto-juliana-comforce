@extends('layouts.app')
@section('content')
    <div id="fondo" class="container">
    	<div class="row">
    		<h4 class="with-border">Proceso</h4>
    		<div class="col-md-4 col-md-offset-7 separacion">
	            <a href="{{ route('procesos.index') }}" class="btn btn-primary pull-right"><i class="fa fa-list-ol" aria-hidden="true"></i> Listar Todo</a>
	        </div>
	        @foreach($procesos as $Proceso)
	    		<div class="col-sm-12">		
					<div class="row">
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Consecutivo Proceso</label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control" value="{{ $Proceso->id}}" disabled>
			                    </div>
			                </div>	                
			    		</div>
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Numero Proceso</label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control" value="{{ $Proceso->numeroProceso}}" disabled>
			                    </div>
			                </div>	                
			    		</div>
			    	</div>	
			    	<div class="row">
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Fecha Creación</label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control" value="{{ $Proceso->fechaProceso}}" disabled>
			                    </div>
			                </div>	                
			    		</div>
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Sede</label>
			                    <div class="col-md-8">
			                        <input type="text" class="form-control" value="{{ $Proceso->nombreSede}}" disabled>
			                    </div>
			                </div>	                
			    		</div>
			    	</div>
					<div class="row">
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Presupuesto (COP)</label>
			                    <div class="col-md-8">
			                        <input id="presupuestoProceso" type="text" class="form-control" value="{{ $Proceso->presupuestoProceso}}" disabled>
			                    </div>
			                </div>	                
			    		</div>
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Presupuesto (USD)</label>
			                    <div class="col-md-8">
			                        <input id="presupuestodolarProceso" type="text" class="form-control" value="{{ $Proceso->presupuestodolarProceso}}" disabled>
			                    </div>
			                </div>	                
			    		</div>
			    	</div>
					<div class="row">
			    		<div class="col-sm-6 separacion">				
			    			<div class="form-group">
			                    <label class="col-md-4 control-label">Descripción</label>
			                    <div class="col-md-8">
			                        <textarea class="form-control" disabled><?php echo  $Proceso->descripcionProceso ?></textarea>
			                    </div>
			                </div>	                
			    		</div>
			    	</div>
			    @endforeach	
    		</div>
    	</div>    	
    </div>
<script src="{{asset('js/proceso.js')}}"></script>     
@endsection