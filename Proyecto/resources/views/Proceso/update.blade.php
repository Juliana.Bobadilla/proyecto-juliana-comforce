@extends('layouts.app')
@section('content')
    <div id="fondo" class="container">
    	<h4 class="with-border">Actualizar Proceso</h4>
    	{!! Form::model($procesos, ['route' => ['procesos.update', $procesos->id],'method' => 'PUT', 'id' => 'form_proceso']) !!}
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Numero Proceso</label>
	                    <div class="col-md-6">
	                        <input id="numeroProceso" type="text" class="form-control" name="numeroProceso" value="{{ $procesos->numeroProceso}}">
	                    </div>
	                </div>	                
	    		</div>
	    	</div> 
			<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Descripción</label>
	                    <div class="col-md-6">
	                        <textarea id="descripcionProceso" class="form-control" name="descripcionProceso"><?php echo  $procesos->descripcionProceso ?></textarea>
	                    </div>
	                </div>	                
	    		</div>
	    	</div> 
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">
					<div class="form-group">
					  	<label class="col-md-4 control-label">Sede</label>
					  	<div class="col-md-6">
	                        <select class="form-control" id="idSede" name="idSede">
							    @foreach($sedes as $Sede)
									@if($Sede->id == $procesos->idSede)
										<option selected value="{{$Sede->id}}">{{$Sede->nombreSede}}</option>
									@else
										<option value="{{$Sede->id}}">{{$Sede->nombreSede}}</option>
									@endif
								@endforeach
							</select>
	                    </div>					  
					</div>	                
	    		</div>
	    	</div> 
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Presupuesto (COP)</label>
	                    <div class="col-md-6">
	                        <input id="presupuestoProceso" type="text" class="form-control" name="presupuestoProceso" value="{{ $procesos->presupuestoProceso}}">
	                    </div>
	                </div>	                
	    		</div>
	    	</div> 
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-5 separacion">
			        <button class="btn btn-primary">Guardar</button>
			    </div>
	    	</div> 
    	{{ Form::close() }} 	
    </div>
<script src="{{asset('js/proceso.js')}}"></script>     
@endsection