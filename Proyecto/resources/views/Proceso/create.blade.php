@extends('layouts.app')
@section('content')
    <div id="fondo" class="container">
    	<h4 class="with-border">Agregar Proceso</h4>
    	{{ Form::open(['route' => 'procesos.store', 'id' => 'form_proceso']) }}
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Numero Proceso</label>
	                    <div class="col-md-6">
	                        <input id="numeroProceso" type="text" class="form-control" name="numeroProceso">
	                    </div>
	                </div>	                
	    		</div>
	    	</div> 
			<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Descripción</label>
	                    <div class="col-md-6">
	                        <textarea id="descripcionProceso" class="form-control" name="descripcionProceso"></textarea>
	                    </div>
	                </div>	                
	    		</div>
	    	</div> 
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">
					<div class="form-group">
					  	<label class="col-md-4 control-label">Sede</label>
					  	<div class="col-md-6">
	                        <select class="form-control" id="idSede" name="idSede">
							    <option value="" readonly selected>Seleccione la Sede</option>
								  @foreach($sedes as $Sede)
								    <option value="{{$Sede->id}}">{{$Sede->nombreSede}}</option>
								  @endforeach
							</select>
	                    </div>					  
					</div>	                
	    		</div>
	    	</div> 
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-3 separacion">				
	    			<div class="form-group">
	                    <label class="col-md-4 control-label">Presupuesto (COP)</label>
	                    <div class="col-md-6">
	                        <input id="presupuestoProceso" type="text" class="form-control" name="presupuestoProceso">
	                    </div>
	                </div>	                
	    		</div>
	    	</div> 
	    	<div class="row">
	    		<div class="col-sm-8 col-md-offset-5 separacion">
			        <button class="btn btn-primary">Guardar</button>
			    </div>
	    	</div> 
    	{{ Form::close() }} 	
    </div>
<script src="{{asset('js/proceso.js')}}"></script>   
@endsection